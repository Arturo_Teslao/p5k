#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO 52
#define MIA_LONGO_DE_NOMOJ_DE_KARTOJ 8
#define MIA_KVANTO_DE_ludantoJ 7
#define MIA_KVANTO_DE_KARTOJ_DE_ludanto 5

const char *Mia_kartaro[MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO] =
  {
    "🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮",
    "🂱", "🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾",
    "🃁", "🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎",
    "🃑", "🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞"
  };
const char *Mia_ludantoj[MIA_KVANTO_DE_ludantoJ][MIA_KVANTO_DE_KARTOJ_DE_ludanto];
char Mia_ludanto_0[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_1[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_2[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_3[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_4[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_5[5] = {'e', 'm', 'p', 't', 'y'};
char Mia_ludanto_6[5] = {'e', 'm', 'p', 't', 'y'};

void Mia_ŝuflado_de_kartaro(const char *Mia_kartaro[], int n)
  {
    for (int i = n - 1; i > 0; i--)
      {
        int j;
        int fd = open("/dev/urandom", O_RDONLY);
        if (fd < 0)
          {
            perror("Ne eblis malfermi /dev/urandom.");
            exit(EXIT_FAILURE);
          }
        if (read(fd, &j, sizeof(j)) != sizeof(j))
          {
            perror("Ne eblis legi de /dev/urandom.");
            close(fd);
            exit(EXIT_FAILURE);
          }
        close(fd);
        j = abs(j) % (i + 1);
        const char *temp = Mia_kartaro[i];
        Mia_kartaro[i] = Mia_kartaro[j];
        Mia_kartaro[j] = temp;
      }
  }

void Mia_printi_la_kartaron(const char *Mia_kartaro[], int n)
  {
    for (int i = 0; i < n; i++)
      {
        printf("%s ", Mia_kartaro[i]);
      }
    printf("\n");
  }

void Mia_disdono_de_kartoj(const char *Mia_kartaro[], const char *Mia_ludantoj[][MIA_KVANTO_DE_KARTOJ_DE_ludanto], int Mia_kvanto_de_ludantoj, int Karto_po_ludanto)
  {
    int cardIndex = 0;
    for (int i = 0; i < Karto_po_ludanto; i++)
      {
        for (int j = 0; j < Mia_kvanto_de_ludantoj; j++)
          {
            if (cardIndex < MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO)
              {
                Mia_ludantoj[j][i] = Mia_kartaro[cardIndex++];
              }
          }
      }
  }

void Mia_printi_la_manojn_de_ludantoj(const char *Mia_ludantoj[][MIA_KVANTO_DE_KARTOJ_DE_ludanto], int Mia_kvanto_de_ludantoj, int Karto_po_ludanto)
  {
    for (int i = 0; i < Mia_kvanto_de_ludantoj; i++)
      {
        printf("ludanto %d: ", i + 1);
        for (int j = 0; j < Karto_po_ludanto; j++)
          {
            printf("%s ", Mia_ludantoj[i][j]);
          }
        printf("\n");
      }
  }

void Mia_printi_la_restantan_kartaron(const char *Mia_kartaro[], int startIndex)
  {
    printf("Rimanta kartaro:\n");
    for (int i = startIndex; i < MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO; i++)
      {
        printf("%s ", Mia_kartaro[i]);
      }
    printf("\n");
  }

int main()
  {
    Mia_ŝuflado_de_kartaro(Mia_kartaro, MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO);

    int Mia_kvanto_de_ludantoj;
    while(1)
      {
        printf("Enigu la kvanton de ludantoj (de 2 ĝis 7): ");
        if (scanf("%d", &Mia_kvanto_de_ludantoj) != 1)
          {
            while (getchar() != '\n');
            printf("Bonvolu enigi numeron..\n");
            continue;
          }
        if (Mia_kvanto_de_ludantoj < 2 || Mia_kvanto_de_ludantoj > 7)
          {
            printf("Malĝusta kvanto de ludantoj. Bonvolu uzi de 2 ĝis 7.\n");
          }
        else
          {
            break;
          }
      }

    printf("Kvanto de ludantoj: %d\n", Mia_kvanto_de_ludantoj);
    Mia_disdono_de_kartoj(Mia_kartaro, Mia_ludantoj, Mia_kvanto_de_ludantoj, MIA_KVANTO_DE_KARTOJ_DE_ludanto);
    Mia_printi_la_manojn_de_ludantoj(Mia_ludantoj, Mia_kvanto_de_ludantoj, MIA_KVANTO_DE_KARTOJ_DE_ludanto);
    Mia_printi_la_restantan_kartaron(Mia_kartaro, MIA_KVANTO_DE_ludantoJ * MIA_KVANTO_DE_KARTOJ_DE_ludanto);

    printf("Ooriginala kartaro:\n");
    Mia_printi_la_kartaron(Mia_kartaro, MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO);

    Mia_ŝuflado_de_kartaro(Mia_kartaro, MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO);

    printf("Peremishita kartaro:\n");
    Mia_printi_la_kartaron(Mia_kartaro, MIA_KVANTO_DE_KARTOJ_EN_LA_KARTARO);

    return 0;
  }
